## My own Hacker News

This is a Hacker News clone created as a front-end coding exercise. It shows
top stories on its home page and a separate comments page. It's been built with
React and CSS modules and uses official Hacker News [Firebase
API](https://github.com/HackerNews/API).

It's hosted on GitLab Pages [jelenica.gitlab.io/hacker-news](https://jelenica.gitlab.io/hacker-news/#/)

### Development instructions

To get this project started locally, you must:
- clone this repo
- install dependencies with `npm install`
- run Webpack dev server with `npm run start`

To create a bundle, use `npm run build`.
