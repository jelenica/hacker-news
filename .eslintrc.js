module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "rules": {
      "react/jsx-filename-extension": [
        1,
        {
          "extensions": [".js", ".jsx"]
        }
      ],
      "jsx-a11y/anchor-is-valid": [
        "error",
        {
          "components": [ "Link" ],
          "specialLink": [ "to" ]
        }
      ],
      "import/prefer-default-export": "off",
    },
    "env": {
      "browser": true,
      "node": true
    },
};
