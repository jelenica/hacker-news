import React from 'react';
import { HashRouter, Route, Link } from 'react-router-dom';
import StoryList from './StoryList';
import Story from './Story';
import styles from './App.css';
import logo from '../images/logo.gif';

const App = () => (
  <HashRouter>
    <div className={styles.container}>
      <div className={styles.header}>
        <Link to="/" className={styles.headerText}>
          <img src={logo} className={styles.logo} alt="Hacker News" />
          <span>Hacker News</span>
        </Link>
      </div>
      <div>
        <Route exact path="/" component={StoryList} />
        <Route path="/story/:storyId" component={Story} />
      </div>
    </div>
  </HashRouter>
);

export default App;
