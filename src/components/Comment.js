import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import TimeAgo from 'react-timeago';
import { getItem } from '../utils/api';
import styles from './Comment.css';
import loading from '../images/loading.gif';

class Comment extends React.Component {
  constructor() {
    super();
    this.getComment = this.getComment.bind(this);
    this.toggleKids = this.toggleKids.bind(this);
    this.renderToggleButton = this.renderToggleButton.bind(this);
    this.renderCommentHeader = this.renderCommentHeader.bind(this);
    this.state = {
      comment: {},
      loaded: false,
      expanded: true,
    };
  }

  componentDidMount() {
    this.getComment();
  }

  getComment() {
    const { id } = this.props;
    getItem(id).then((comment) => {
      this.setState({
        comment: comment.data,
        loaded: true,
      });
    });
  }

  toggleKids() {
    const expanded = !this.state.expanded;
    this.setState({ expanded });
  }

  renderToggleButton() {
    const expanded = '[ - ]';
    const collapsed = '[ + ]';
    return (
      <span
        onClick={() => this.toggleKids()}
        onKeyUp={() => this.toggleKids()}
        role="button"
        tabIndex="-1"
        className={styles.toggleButton}
      >
        {this.state.expanded ? expanded : collapsed}
      </span>
    );
  }

  renderCommentHeader() {
    const { comment } = this.state;
    return (
      <div className={styles.header}>
        <span className={styles.author}>{comment.by} </span>
        <TimeAgo date={comment.time * 1000} />
        {this.renderToggleButton()}
      </div>
    );
  }

  renderComment() {
    const { comment, expanded } = this.state;
    const { level } = this.props;
    return (!comment.deleted &&
      <div>
        {this.renderCommentHeader()}
        <CSSTransition
          in={expanded}
          timeout={300}
          classNames={styles}
          mountOnEnter
        >
          <div>
            <div
              dangerouslySetInnerHTML={{ __html: comment.text }}
              className={styles.text}
            />
            { comment.kids &&
              <div className={styles.kids}>
                {comment.kids.map(id =>
                  (<Comment key={id} id={id} level={level + 1} />))
                }
              </div>
            }
          </div>
        </CSSTransition>
      </div>
    );
  }

  render() {
    const { loaded } = this.state;
    return (
      <div className={styles.container}>
        { loaded ? (
          this.renderComment()
        ) : (
          <img src={loading} className={styles.loadImage} alt="" />
        ) }
      </div>
    );
  }
}

Comment.propTypes = {
  id: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
};

export default Comment;
