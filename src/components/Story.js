import React from 'react';
import PropTypes from 'prop-types';
import { getItem } from '../utils/api';
import StoryHeader from './StoryHeader';
import StorySubtitle from './StorySubtitle';
import Comment from './Comment';
import styles from './Story.css';

class Story extends React.Component {
  constructor() {
    super();
    this.getStory = this.getStory.bind(this);
    this.state = {
      story: {},
      loaded: false,
    };
  }

  componentDidMount() {
    this.getStory();
  }

  getStory() {
    const id = this.props.match.params.storyId;
    getItem(id).then((story) => {
      this.setState({
        story: story.data,
        loaded: true,
      });
    });
  }

  render() {
    const comments = this.state.story.kids;
    const { loaded } = this.state;
    const {
      title, url, score, time, by,
    } = this.state.story;
    return (loaded &&
      <div className={styles.container}>
        <StoryHeader title={title} url={url} />
        <StorySubtitle score={score} time={time} by={by} />
        <div className={styles.comments}>
          {
            comments && comments.map(id =>
              (<Comment key={id} id={id} level={0} />))
          }
        </div>
      </div>
    );
  }
}

Story.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      storyId: PropTypes.string,
    }),
  }).isRequired,
};

export default Story;
