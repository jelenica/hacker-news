import React from 'react';
import { getStoryIds, getStoriesPage } from '../utils/api';
import StoryListItem from './StoryListItem';
import styles from './StoryList.css';
import loading from '../images/loading.gif';

class StoryList extends React.Component {
  constructor() {
    super();
    this.getStoriesPage = this.getStoriesPage.bind(this);
    this.initStories = this.initStories.bind(this);
    this.state = {
      loaded: false,
      storyIds: [],
      stories: [],
      currentPage: 0,
    };
  }

  componentDidMount() {
    this.initStories();
  }

  getStoriesPage() {
    let { stories, currentPage } = this.state;
    const { storyIds } = this.state;
    this.setState({ loaded: false });
    getStoriesPage(storyIds, currentPage).then((newStories) => {
      stories = stories.concat(newStories);
      currentPage += 1;
      this.setState({
        stories,
        currentPage,
        loaded: true,
      });
    });
  }

  initStories() {
    getStoryIds().then((response) => {
      this.setState({ storyIds: response.data });
      this.getStoriesPage();
    });
  }

  render() {
    const { loaded } = this.state;
    const loadMoreButton = (
      <button
        onClick={() => this.getStoriesPage()}
        className={styles.loadButton}
      >
        Load more
      </button>
    );
    return (
      <div className={styles.container}>
        {
          this.state.stories.map(story =>
            <StoryListItem story={story.data} key={story.data.id} />)
        }
        { (loaded) ? (
          loadMoreButton
        ) : (
          <img src={loading} className={styles.loadImage} alt="" />
        )}
      </div>
    );
  }
}

export default StoryList;
