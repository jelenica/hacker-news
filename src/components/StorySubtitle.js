import React from 'react';
import TimeAgo from 'react-timeago';
import PropTypes from 'prop-types';
import styles from './StorySubtitle.css';

const StorySubtitle = props => (
  <div className={styles.container}>
    <span>
      {`${props.score} points by ${props.by} `}
    </span>
    <TimeAgo date={props.time * 1000} />
  </div>
);

StorySubtitle.propTypes = {
  score: PropTypes.number,
  time: PropTypes.number,
  by: PropTypes.string,
}.isRequired;

export default StorySubtitle;
