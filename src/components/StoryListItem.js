import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import StoryHeader from './StoryHeader';
import StorySubtitle from './StorySubtitle';
import styles from './StoryListItem.css';

class StoryListItem extends React.Component {
  constructor() {
    super();
    this.renderCommentLink = this.renderCommentLink.bind(this);
  }

  renderCommentLink() {
    const { id, descendants } = this.props.story;
    const linkText = `${descendants} ${descendants === 1 ? 'comment' : 'comments'}`;
    return (descendants >= 0 &&
      <span>
        <span className={styles.divider}>|</span>
        <Link to={`/story/${id}`} className={styles.commentLink}>{linkText}</Link>
      </span>
    );
  }

  render() {
    const {
      title, url, by, score, time,
    } = this.props.story;
    return (
      <div className={styles.container}>
        <StoryHeader title={title} url={url} />
        <StorySubtitle score={score} time={time} by={by} />
        { this.renderCommentLink() }
      </div>
    );
  }
}

StoryListItem.propTypes = {
  story: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string,
    by: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    time: PropTypes.number.isRequired,
    descendants: PropTypes.number,
  }),
};

StoryListItem.defaultProps = {
  story: {
    descendants: 0,
    url: '',
  },
};

export default StoryListItem;
