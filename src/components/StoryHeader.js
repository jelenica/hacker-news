import React from 'react';
import PropTypes from 'prop-types';
import { parseUrl } from '../utils/helpers';
import styles from './StoryHeader.css';

const StoryHeader = props => (
  <div className={styles.container}>
    <div className={styles.title}>
      <a href={props.url} target="_blank">{props.title}</a>
    </div>
    <span className={styles.url}>
      ({parseUrl(props.url)})
    </span>
  </div>
);

StoryHeader.propTypes = {
  title: PropTypes.string,
  url: PropTypes.string,
}.isRequired;

export default StoryHeader;
