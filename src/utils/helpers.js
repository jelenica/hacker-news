import urlParse from 'url-parse';

export function parseUrl(url) {
  return urlParse(url).hostname;
}
