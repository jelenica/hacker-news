import axios from 'axios';

const pageLength = 30;

export function getItem(id) {
  return axios.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json`);
}

export function getStoryIds() {
  return axios.get('https://hacker-news.firebaseio.com/v0/topstories.json');
}

export function getStoriesPage(ids, page) {
  const start = page * pageLength;
  const end = start + pageLength;
  const stories = ids.slice(start, end).map(id => getItem(id));
  return Promise.all(stories);
}
